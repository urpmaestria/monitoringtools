package com.emedinaa.performance.sample.adapter

/**
 * @author Eduardo Medina
 */
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.performance.sample.R
import com.emedinaa.performance.sample.model.User

class UserAdapter(private var users: List<User>) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    fun clean() {
        users = emptyList()
        notifyDataSetChanged()
    }

    fun update(items: List<User>) {
        users = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_user,
            parent, false
        )
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount(): Int = users.size

    inner class UserViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val textViewUsername = view.findViewById<TextView>(R.id.textViewUsername)
        private val textViewFullName = view.findViewById<TextView>(R.id.textViewFullname)

        fun bind(entity: User) {
            with(entity) {
                textViewUsername.text = username
                textViewFullName.text = "$firstname $lastname"
            }

            view.setOnClickListener {
                Log.v("CONSOLE", "entity $entity")
            }
        }
    }
}