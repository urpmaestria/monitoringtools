package com.emedinaa.performance.sample.data.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Eduardo Medina
 */
private const val URL = "https://obscure-earth-55790.herokuapp.com"

class ApiClient {

    private lateinit var serviceApi: Service

    private val interceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }


    fun build(): Service {
        val builder = Retrofit.Builder().apply {
            this.baseUrl(URL)
            this.addConverterFactory(GsonConverterFactory.create())
        }

        //TODO development
        val httpClient = OkHttpClient.Builder().let {
            it.addInterceptor(interceptor)
            it.build()
        }

        val retrofit = builder.let {
            it.client(httpClient)
            it.build()
        }
        serviceApi = retrofit.create(Service::class.java)

        return serviceApi
    }

    interface Service {

        @GET("/api/notes/user/{userId}")
        suspend fun notesByUser(@Path("userId") userId: String): Response<NotesResponse>

        @GET("/api/notes")
        suspend fun notes(): Response<NotesResponse>

        @GET("/api/users")
        suspend fun users(): Response<UsersResponse>
    }
}