package com.emedinaa.performance.sample

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController

/**
 * @author Eduardo Medina
 */
class MainFragment : Fragment() {

    private var value: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.button).setOnClickListener {
            if (Helpers.validateInputText(view.findViewById(R.id.editTextValue))) {
                goToServices()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.crash_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.crashMenu -> {
                makeCrash()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun makeCrash() {
        val input = view?.findViewById<EditText>(R.id.editTextValue)?.text.toString()
        Log.d("CONSOLE", "input $input")
        value = input.ifEmpty { null }
        Log.d("CONSOLE", "value length ${value!!.length}")
    }

    private fun goToServices() {
        //findNavController().navigate(R.id.action_mainFragment_to_servicesFragment)
        startActivity(Intent(requireContext(), ServicesActivity::class.java))
        requireActivity().finish()
    }
}