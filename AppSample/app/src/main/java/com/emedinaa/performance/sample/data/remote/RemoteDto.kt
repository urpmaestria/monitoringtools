package com.emedinaa.performance.sample.data.remote

import com.emedinaa.performance.sample.model.Note
import com.emedinaa.performance.sample.model.User

/**
 * @author Eduardo Medina
 */

data class UserDto(
    val id: String?, val username: String?, val firstname: String?,
    val lastname: String?
) {
    fun toUser() = User(id?:"", username ?: "", firstname ?: "", lastname ?: "")
}

data class NoteDto(
    val id: String, val title: String?, val description: String?,
    val timestamp: String?
) {
    fun toNote() = Note(id, title ?: "", description ?: "", timestamp ?: "")
}

data class NotesResponse(
    val status: Int, val msg: String = "",
    val data: List<NoteDto>
)

data class UsersResponse(
    val status: Int, val msg: String = "",
    val data: List<UserDto>
)

