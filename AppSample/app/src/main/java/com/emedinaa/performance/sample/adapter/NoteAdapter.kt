package com.emedinaa.performance.sample.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.performance.sample.R
import com.emedinaa.performance.sample.model.Note

/**
 * @author Eduardo Medina
 */
class NoteAdapter(private var notes: List<Note>) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    fun update(items: List<Note>) {
        notes = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_note,
            parent, false
        )
        return NoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(notes[position])
    }

    override fun getItemCount(): Int = notes.size

    inner class NoteViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val textViewTitle = view.findViewById<TextView>(R.id.textViewTitle)
        private val textViewDesc = view.findViewById<TextView>(R.id.textViewDesc)

        fun bind(entity: Note) {
            with(entity) {
                textViewTitle.text = title
                textViewDesc.text = description
            }

            view.setOnClickListener {
                Log.v("CONSOLE", "entity $entity")
            }
        }
    }
}