package com.emedinaa.performance.sample.model


/**
 * @author Eduardo Medina
 */
data class User(
    val id: String, val username: String, val firstname: String,
    val lastname: String
)

data class Note(
    val id: String, val title: String, val description: String,
    val timestamp: String
)
