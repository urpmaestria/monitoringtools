package com.emedinaa.performance.sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * @author Eduardo Medina
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}