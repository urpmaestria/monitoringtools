package com.emedinaa.performance.sample

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.performance.sample.adapter.NoteAdapter
import com.emedinaa.performance.sample.adapter.UserAdapter
import com.emedinaa.performance.sample.data.NoteRepository
import com.emedinaa.performance.sample.data.UserRepository
import com.emedinaa.performance.sample.data.remote.ApiClient
import com.emedinaa.performance.sample.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Eduardo Medina
 */
class ServicesFragment : Fragment() {

    private val noteRepository = NoteRepository(ApiClient())
    private val userRepository = UserRepository(ApiClient())

    private var noteAdapter: NoteAdapter = NoteAdapter(emptyList())
    private var userAdapter: UserAdapter = UserAdapter(emptyList())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_services, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUi()
        fetchData()
    }

    private fun setUi() {
        view?.findViewById<RecyclerView>(R.id.recyclerViewNotes)?.adapter = noteAdapter
        view?.findViewById<RecyclerView>(R.id.recyclerViewUsers)?.adapter = userAdapter
    }

    private fun showUserViewLoading() {
        //view?.findViewById<View>(R.id.progressBarUser)?.visibility = View.VISIBLE
        Helpers.showViewLoading(view?.findViewById(R.id.progressBarUser))
    }

    private fun hideUserViewLoading() {
        //view?.findViewById<View>(R.id.progressBarUser)?.visibility = View.GONE
        Helpers.hideViewLoading(view?.findViewById(R.id.progressBarUser))
    }

    private fun showNoteViewLoading() {
        //view?.findViewById<View>(R.id.progressBarNote)?.visibility = View.VISIBLE
        Helpers.showViewLoading(view?.findViewById(R.id.progressBarNote))
    }

    private fun hideNoteViewLoading() {
        //view?.findViewById<View>(R.id.progressBarNote)?.visibility = View.GONE
        Helpers.hideViewLoading(view?.findViewById(R.id.progressBarNote))
    }

    private fun fetchData() {
        lifecycleScope.launch {
            showUserViewLoading()
            var data: List<User> = emptyList()
            for (i in 0..20) {
                val result = withContext(Dispatchers.Main) {
                    userRepository.getUsers()
                }
                if (result.isSuccess) {
                    data = result.getOrDefault(emptyList())
                    userAdapter.clean()
                    userAdapter.update(data)
                } else {
                    //result.exceptionOrNull()
                }
            }
            showNoteViewLoading()
            data.filter { it.id.isNotEmpty() }.forEach {
                val resultNotes = withContext(Dispatchers.Main) {
                    noteRepository.getNotesByUser(it.id)
                }
                if (resultNotes.isSuccess) {
                    noteAdapter.update(resultNotes.getOrDefault(emptyList()))
                } else {
                    //result.exceptionOrNull()
                }
            }

            val resultAllNotes = withContext(Dispatchers.Main) {
                noteRepository.getNotes()
            }
            if (resultAllNotes.isSuccess) {
                noteAdapter.update(resultAllNotes.getOrDefault(emptyList()))
            } else {
                //result.exceptionOrNull()
            }
            hideUserViewLoading()
            hideNoteViewLoading()
        }
    }

    private fun fetchNotes() {
        showNoteViewLoading()
        lifecycleScope.launch {
            val result = withContext(Dispatchers.Main) {
                noteRepository.getNotes()
            }
            if (result.isSuccess) {
                noteAdapter.update(result.getOrDefault(emptyList()))
            } else {
                //result.exceptionOrNull()
            }
            hideNoteViewLoading()
        }
    }
}