package com.emedinaa.performance.sample.data

import com.emedinaa.performance.sample.data.remote.ApiClient
import com.emedinaa.performance.sample.data.remote.NotesResponse
import com.emedinaa.performance.sample.data.remote.UsersResponse
import com.emedinaa.performance.sample.model.Note
import com.emedinaa.performance.sample.model.User

/**
 * @author Eduardo Medina
 */

class UserRepository(apiClient: ApiClient) {
    private val api = apiClient.build()

    suspend fun getUsers(): Result<List<User>> {
        return try {
            val response = api.users()
            if (response.isSuccessful) {
                val body: UsersResponse? = response.body()
                val users: List<User> = body?.data?.map {
                    it.toUser()
                } ?: emptyList()
                Result.success(users)
            } else {
                val errorBody = response.errorBody()?.string()
                Result.failure(Exception(errorBody))
            }
        } catch (exception: Exception) {
            Result.failure(exception)
        }
    }
}