package com.emedinaa.performance.sample.data

import com.emedinaa.performance.sample.data.remote.ApiClient
import com.emedinaa.performance.sample.data.remote.NotesResponse
import com.emedinaa.performance.sample.model.Note
import com.emedinaa.performance.sample.model.User

/**
 * @author Eduardo Medina
 */

class NoteRepository(private val apiClient: ApiClient) {

    private val api = apiClient.build()

    suspend fun getNotes(): Result<List<Note>> {
        return try {
            val response = api.notes()
            if (response.isSuccessful) {
                val body: NotesResponse? = response.body()
                val notes: List<Note> = body?.data?.map {
                    it.toNote()
                } ?: emptyList()
                Result.success(notes)

            } else {
                val errorBody = response.errorBody()?.string()
                Result.failure(Exception(errorBody))
            }
        } catch (exception: Exception) {
            Result.failure(exception)
        }
    }

    suspend fun getNotesByUser(userId: String): Result<List<Note>> {
        return try {
            val response = api.notesByUser(userId)
            if (response.isSuccessful) {
                val body: NotesResponse? = response.body()
                val notes: List<Note> = body?.data?.map {
                    it.toNote()
                } ?: emptyList()
                Result.success(notes)

            } else {
                val errorBody = response.errorBody()?.string()
                Result.failure(Exception(errorBody))
            }
        } catch (exception: Exception) {
            Result.failure(exception)
        }
    }
}