package com.emedinaa.performance.sample

import android.view.View
import android.widget.EditText

/**
 * @author Eduardo Medina
 */
object Helpers {
    private var editText: EditText? = null
    private var progressBar: View? = null

    fun validateInputText(editText: EditText): Boolean {
        this.editText = editText
        val text = this.editText?.text.toString().trim()
        return text.isNotEmpty()
    }

    fun showViewLoading(progressBar: View?) {
        this.progressBar = progressBar
        this.progressBar?.visibility = View.VISIBLE
    }

    fun hideViewLoading(progressBar: View?) {
        this.progressBar = progressBar
        this.progressBar?.visibility = View.GONE
    }
}